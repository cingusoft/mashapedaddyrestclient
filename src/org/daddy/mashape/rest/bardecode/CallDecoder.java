package org.daddy.mashape.rest.bardecode;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import org.daddy.mashape.models.Decoder;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class CallDecoder {
	private RestTemplate restTemplate;
	private Decoder decoder;
	public CallDecoder(String imageFile){
		this._init();
		//open Image File
		try{
			File file = new File(imageFile);
			FileInputStream fis = new FileInputStream(file);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			try{
				 for (int readNum; (readNum = fis.read(buf)) != -1;) {
		                bos.write(buf, 0, readNum);
		                System.out.println("read " + readNum + " bytes,");
		            }
		    } catch (IOException ex) {
		            System.out.println("Error");
		    }
		    String encodedImage = Base64.encodeBase64String(bos.toByteArray());
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("barcode", encodedImage);
			decoder = restTemplate.postForObject("http://localhost:8080/Mashape/bardecode/", map, Decoder.class);
			System.out.println(decoder.getResult());
		}catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
				
	}
	private void _init(){
		restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> mc = restTemplate.getMessageConverters();
		MappingJacksonHttpMessageConverter json = new MappingJacksonHttpMessageConverter();
		List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
		supportedMediaTypes.add(new MediaType("text","javascript"));
		json.setSupportedMediaTypes(supportedMediaTypes);
		mc.add(json);
		restTemplate.setMessageConverters(mc);
	}
}
