package org.daddy.mashape.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.daddy.mashape.models.Token;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;


public class RequestToken {
	
	private RestTemplate restTemplate;
	private Token token;
	
	public RequestToken(String devKey){
		this._init();
		Map<String, String> map = new HashMap<String, String>();
		map.put("devkey", devKey);
		token = restTemplate.getForObject("https://D0enE2uaHlo4NzlvExPL1PxO9.proxy.mashape.com/requestToken?devkey={devkey}",Token.class, map);
		System.out.println(token.getToken());
	}
	public String getToken(){
		return token.getToken();
	}
	
	
	private void _init(){
		restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> mc = restTemplate.getMessageConverters();
		MappingJacksonHttpMessageConverter json = new MappingJacksonHttpMessageConverter();
		List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
		supportedMediaTypes.add(new MediaType("text","javascript"));
		json.setSupportedMediaTypes(supportedMediaTypes);
		mc.add(json);
		restTemplate.setMessageConverters(mc);
	}
}
