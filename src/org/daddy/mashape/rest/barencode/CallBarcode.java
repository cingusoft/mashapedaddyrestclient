package org.daddy.mashape.rest.barencode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.daddy.mashape.models.Barcode;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class CallBarcode {
	private RestTemplate restTemplate;
	private Barcode barcode;
	
	public CallBarcode(String barcodeType, String message, String token){
		this._init();
		Map<String, String> map = new HashMap<String, String>();
		map.put("barcode", barcodeType);
		map.put("token", token);
		map.put("message", message);
		barcode = restTemplate.getForObject("http://mashape.cloudfoundry.com/barencode/{barcode}/{message}?_token={token}",Barcode.class, map);
		
		System.out.println(barcode.getData());
		System.out.println(barcode.getStatus());
	}
	private void _init(){
		restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> mc = restTemplate.getMessageConverters();
		MappingJacksonHttpMessageConverter json = new MappingJacksonHttpMessageConverter();
		List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
		supportedMediaTypes.add(new MediaType("text","javascript"));
		json.setSupportedMediaTypes(supportedMediaTypes);
		mc.add(json);
		restTemplate.setMessageConverters(mc);
	}
}
