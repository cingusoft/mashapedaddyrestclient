package org.daddy.mashape.projects;

import org.daddy.mashape.rest.RequestToken;
import org.daddy.mashape.rest.barencode.CallBarcode;
import org.daddy.mashape.rest.barencode.CallQrCode;

public class Barencode {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RequestToken request = new RequestToken("vNCZRR9DeGwB772Ckw45n6i0g");
		CallQrCode barcode = new CallQrCode("this is a QR Code Test", request.getToken());
		System.out.println(barcode);
	}

}
