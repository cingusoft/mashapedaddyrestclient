package org.daddy.mashape.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Token {
	private String _token;
	
	public void setToken(String token){
		this._token=token;
	}
	public String getToken(){
		return this._token;
	}
}
