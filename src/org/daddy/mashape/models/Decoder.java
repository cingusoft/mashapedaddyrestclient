package org.daddy.mashape.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Decoder {
	private String _result;
	
	public void setResult(String result){
		this._result=result;
	}
	public String getResult(){
		return this._result;
	}
}
