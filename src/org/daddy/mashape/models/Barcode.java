package org.daddy.mashape.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Barcode {
	private String _data;
	private String _status;
	
	
	public void setData(String data){
		this._data = data;
	}
	public String getData(){
		return this._data;
	}
	
	public void setStatus(String status){
		this._status=status;
	}
	public String getStatus(){
		return this._status;
	}
}
